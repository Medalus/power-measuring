                                                                   **Requirements draft** 

                                                                   _UPS Load Factor _
**Issues**

**Design system**

- 	Drawing diagram 
- 	Create system schematic 
- 	Simulate 
- 	Build prototype 


**Setup environment **

- 	Choosing an environment 
- 	Getting required add ons 
- 	Setup virtual environment


**Coding - Pi **

- 	Import libraries 
- 	Make classes 
- 	Instantiate classes 
- 	The rest of the code is written 
- 	Debug 


**Coding - Microcontroller **

- 	Import libraries 
- 	Make classes (unless C code) 
- 	Instantiate classes (unless C code) 
- 	The rest of the code is written 
- 	Debug 


**Consolidate **

- 	Test system 
- 	Create enclosure 
- 	Update and improve Documentation 6. Make it useful 
- 	Writing report 
- 	Writing User guide 
- 	Writing Manual


**Post-launch **
-  Post mortem 
 
**Components**

- ESP8266 
- Raspberry Pi 
- Sensors (Voltage, waveform and frequency) 
- Protocols 
- MQTT 
- MQ Telemetry Transport communication protocol to be used to send and receive messages across the network.  
- Coding language and environment


**On Raspberry Pi: **

Python - Visual studio/Pycharm On esp8266:
 
- 	Micropython - Visual studio/Pycharm/(Arduino IDE) 
- 	C++ with arduino libs - Visual Studio/Arduino IDE 
- 	C with RTOS - Visual Studio/Arduino IDE 
  

