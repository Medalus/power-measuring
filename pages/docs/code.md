# Code
The code in this project consists of multiple parts, and therefore multiple code scripts. 
They are all written in python, and focuses on OOP (Object Oriented Programming). 

When referenced, these are the links to the python code files:<br>
- <a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/Finalized%20project%20code/LED.py"> LED.py</a><br>
- <a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/Finalized%20project%20code/MQTT.py"> MQTT.py </a><br>
- <a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/Finalized%20project%20code/calculation.py"> calculations.py </a><br>
- <a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/Finalized%20project%20code/controller.py"> controller.py </a><br>
- <a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/Finalized%20project%20code/error_checking.py"> error_checking.py </a><br>
- <a href="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/blob/master/Finalized%20project%20code/sensor_reading.py"> sensor_reading.py </a>

The code as you can see, is split into 6 parts, each talking to eachother. This page will briefly explain each file/section in mentioned order.<br>
To avoid filling this page with snippets and images, please refer to aforementioned links and files.

### LED
___
The first thing to note is that the library <a href="https://gpiozero.readthedocs.io/en/stable/" alt="Link to gpio zero library">gpiozero</a>,
which allows us to use and control our LED easily. 

After that a class called "error_LED" is made to contain everything. The next 5 functions created helps us control the LED by: 
1) initializing it
2) Make it able to blink (1 time per second)
3) Make it able to blink with a different pattern (2 times per second)
4) Make it able to turn on constantly if there is an error
5) Turn the LED off all together
<br><br>
### MQTT
___
Just like with the LED script, the first thing that needs to be done is to import the <a href="https://pypi.org/project/paho-mqtt/" alt="mqtt library">paho.mqtt.client</a> library,
but only taking and using the "mqtt" section.

A class called "mqtt_pub" is instantiated to call the functions contained in it. It also quickly indicates that the mqtt script is used for publishing.

3 functions helps us:
1) Define the topics and connect to the client
2) Publishing the values
3) Publishing any relevant error values/messages
<br><br>
### calculation
___
First off, the library <a href="https://numpy.org/" alt="Link should have been to numpy documentation">numpy</a> is imported.

A class called "calculations" again helps us reference and contain 2 functions which can be called to:
1) Find the mean of whatever is inputted
2) Find the difference between two inputs and makes sure the number is positive
<br><br>
### error_checking
___
No library is imported this time, but a class called "error_checking" is instantiated, containing the 2 functions that:
1) Checks if the value/difference is too high, and triggers an error
2) Checks whether or not the values are within a desired range
<br><br>
### sensor_reading
___
To begin with, three libraries are imported: <a href="https://numpy.org/" alt="Link should have been to numpy documentation">numpy</a>,
<a href="https://docs.python.org/3/library/time.html" alt="Link should have been to time documentation">time</a>, and <a href="https://pypi.org/project/spidev/" alt="Link should have been to spidev documentation">spidev</a>.

A single class called "sensor" is instantiated, containing 2 function. These functions: 
1) Initializes the script, and sets the SPI settings
2) Reads the ADC and sensor values 
<br><br>
### controller
___
This is the main script, collecting everything that has been listed and described so far. The **controller** imports the LED, MQTT, calculations, error_checking, and sensor_reading scripts.

The main class that controls the whole thing is "controller". It contains 3 functions that:
1) Initializes everything
2) Populates the buffer
3) Runs the main loop, which is responsible for collecting the sensor values, checking for errors, publishing via MQTT, and making the LED react in case of errors.
