# Sensors
## Sensor requirements

Sorted list with sensor requirements, most important to least important:  
1. Accuracy - Depends on sensor and application  
2. interference resistivity - yes  
3. Durability - At least 5 years  
4. Size - Bigger than cable for hall sensor, everything else, as small as possible  
5. Price - Low  
6. Response Speed - at least 50Hz  
7. Range - Depends on the sensor and application  
8. Energy consumption - aiming for max 12mA (I/O IMAX of the esp8266)  
9. Replaceability - Yes  
10. Operational relative humidity - 40% - 60% rH  
11. Operational temperature - 18-27C(room temp)(40C near rack outtake)  
  
## Used sensor
The Nuvotem Talema AS-103 current transformer is used to sense current in the measured wires.  
It is used due to its costs and performance.  
  
## Other components
A MCP3202 ADC acts as a "bridge" between the AS-103 current transformer and the Raspberry PI.  
A generic red 5mm LED is used to display error codes.