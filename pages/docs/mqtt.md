## MQTT
MQTT is the messaging protocol used widely within the world of IoT, Internet of Things. 
The documentation can be found here: https://mqtt.org/

MQTT is split up into 3 areas:
1) Broker
2) Publisher
3) Subscriber

#### Broker
The Broker is the intermediary between the sender (publisher) and the receiver (subscriber). 
In this project, we will be using a dedicated computer that is hosting the broker. An identical "topic" is needed from both the publisher
and the subscriber, to make sure that what you receive is what you are looking for.
<br> An option is to set and declare a username and password in order to secure the data transaction. It is not the most secure way
to simply put a username and password on the data, but it offers more than none.

#### Publisher
The MQTT Publisher is what is is being used to send (publish) the data. The client compresses the data it wants to send into a payload,
and with a designated topic publishes the data to the broker. The same publisher can send data via more than one topic, 
which is what we are using to dissern errors from correct sensor readings.

#### Subscriber 
The Subscriber is the one collecting the data sent from the publisher. In our case, the subscriber is Node-RED which helps us display
the data on a GUI (Graphical User Interface). The subscriber just like the publisher, needs to have the correct topic in order to
collect and receive data from the publisher.