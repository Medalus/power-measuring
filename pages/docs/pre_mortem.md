##  Pre mortem, what could go wrong?

<p>Let us imagine, the project is done. Now, there were several things, that went wrong. What were those things?</p>

###  Here is what went wrong:
<p>1. The epics were not cut down enough, they were too ambitious.</p>
<p>2. We missed deadlines, and failed to deliver on time. We took on too small/big tasks, and went over our timelimits for those individual projects/tasks.</p>
<p>3. Team members did not show up for some unknown reason on several occasions. That resulted in lack of knowledge which were required to move forward.</p>
<p>4. The team lacked engagement, and were not passionate enough.</p>
<p>5. The Corona-virus came in the way. There were restrictions and limitations, and team members were infected and unavailable.</p>
<p>6. Overconfidence. The team did not think the tasks through, and thought tasks were easier than they were.</p>
<p>7. Bad communication between team members. They ended up having heated arguments and talked past each other.</p>
<p>8. We had hardware malfunction.</p>
<p>9. We ended up making our things more complicated than they had to be.</p>
<p>10. After the project were done, we forgot to hand it in. We kept on working on it afterwards, trying to refine it over and over.</p>
<p>11. The team did not keep up with team meetings and planning.</p>
<p>12. One or more from the team dropped out, leaving the team thinned.</p>
<p>13. Internet services we were dependent on crashed, and we lost access to them.</p>
<p>14. The tasks were not clear enough, which ended up having a different result than expected.</p>

###  Here is what we could do to avoid the problems:
<p>1. We had split up the epics better, in smaller issues.</p>
<p>2. A clear deadline and having a clear and certain dedicated amount of time allocated for each task. If a task were done prematurely, the spare time could have been added to more difficult tasks.</p>
<p>3. Everybody upheld their promise to show up. If they could not show up for class, they should do the exercises, and be as prepared as possible for next meetup.</p>
<p>4. Finding a way to keep the team passionate about the project - agree in the planning phase of the project, how to make it exciting to do. </p>
<p>5. Team members took extra precautions, and stayed home whenever it was possible.</p>
<p>6. Before starting the work, make sure we all know how to do every aspect of the task. What would it entail? Is that even possible? Does it involve too much work?</p>
<p>7. Keep it civil. If there is anything that really upsets one of the members, dicussing it as a team is the solution. Make sure everyone is heard - respect is the key, no matter what we do.</p>
<p>8. Backup hardware. Make sure there is multiple units, in case something breaks or burns out.</p>
<p>9. This one ties in with overconfidence. Before we started working with it, make sure we agree on what is actually needed. Maybe there are several ways to do the same thing, and maybe some are better than others, while some might be more simple than other.</p>
<p>10. Realise when something is done. When we achieve the *definition of done*, we should keep that in mind.</p>
<p>11. Having our weekly team meetings, while making sure to update our issue boards. Have some points to talk about, even though there might not be anything new. Having a written team meeting agenda each week is essential.</p>
<p>12. Sadly, there is not a lot to do about this. This one is hard to fix. Everyone else suddenly has to do more work, so a solution would be to look at our project, and have a new *first sprint* planning meeting, and keeping this in mind.</p>
<p>13. Have things two places. Local and outside, always.</p>
<p>14. Make sure that everyone are 100% sure what the tasks are and entail. The *definition of done* is a great way to keep us on track.</p>
