# Measuring current
## Using the Hall effect
For best sensing, the magnet should be perpendicular to the sensing area.  
  
Hall sensors are used in many different places, such as combustion engines, DC brushless motors, and so on. In the data center case, it can/will be used in measuring current drops due to changes in the magnetic field.  
There are both open-loop and closed-loop hall effect current transducer types:
<img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/raw/master/pages/img/open_loop.png" alt="Open-loop hall effect current transducer"><br>
<img src="https://gitlab.com/21s-itt2-datacenter-students-group/team-b2-group/power-measuring/-/raw/master/pages/img/closed_loop.png" alt="Closed-loop hall effect current transducer"><br>
source: <a href="https://youtu.be/RrQA_-YETJw">How Hall Effect Current Transducer Works</a><br>
The project is using a Current transformer.