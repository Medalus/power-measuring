import collections
import MQTT
import calculations
import LED
import error_checking
import sensor_reading


class controller:

    def __init__(self):
        # setup
        self.sensor = sensor_reading.sensor(0)
        self.mqtt_publisher = MQTT.mqtt_pub()
        self.calculator = calculations.calculations()
        self.LED = LED.error_LED(11)
        self.error_check = error_checking.error_checking()
        self.buffer0 = collections.deque(maxlen=5)
        self.buffer1 = collections.deque(maxlen=5)

    def populate_buffer(self, adc_pin):
        count = 0
        temp_buffer = []
        while count < 5:
            temp_buffer.append(self.sensor.read_adc(adc_pin))
            count += 1
        return temp_buffer


    def main_loop(self):

        self.buffer0 = self.populate_buffer(0)
        self.buffer1 = self.populate_buffer(1)

        while True:
            # Before you start, yes, I know all of this duplicity should have been inside one function
            # where one would simply call the sensor to measure stuff from
            # But I'm too tired to care right now
            self.buffer0.append(self.sensor.read_adc(0))
            self.buffer1.append(self.sensor.read_adc(1))

            mean_of_buffer0 = self.calculator.get_mean(self.buffer0)
            mean_of_buffer1 = self.calculator.get_mean(self.buffer1)

            sens0_outside_range = self.error_check.outside_range(mean_of_buffer0)
            if sens0_outside_range[0]:
                self.LED.blink_threshold()
                self.mqtt_publisher.send_error(sens0_outside_range[1] + ", Error occured on sensor 0")

            sens1_outside_range = self.error_check.outside_range(mean_of_buffer1)
            if sens1_outside_range[0]:
                self.LED.blink_threshold()
                self.mqtt_publisher.send_error(sens1_outside_range[1] + ", Error occured on sensor 1")
            sens_diff = self.error_check.diff_between_sensors(self.calculator.check_diff(self.buffer0, self.buffer1))
            if sens_diff[0]:
                self.LED.blink_difference()
                self.mqtt_publisher.send_error(sens_diff[1])

            # Not sure this below here is correct...
            self.mqtt_publisher.publish_value(mean_of_buffer0)
            self.mqtt_publisher.publish_value(mean_of_buffer1)
