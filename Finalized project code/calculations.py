import numpy

class calculations():

    def get_mean(self, sensor_value_list):
        # Takes whatever is inputted, then gets the mean of said thing
        return numpy.mean(sensor_value_list)


    def check_diff(self, sensor1_mean, sensor2_mean):
        # Takes two inputs, gets the difference, then makes sure the number is positive
        difference = abs(((sensor1_mean)/(sensor2_mean))*100)
        return difference
