import paho.mqtt.client as mqtt

class mqtt_pub:

    def __init__(self):
        self.topic_values = "B2"
        self.topic_errors = "B2-E"  # Error messages
        self.address = "hobelab-laptop.duckdns.org"
        self.client = mqtt.Client("Pi")
        self.client.username_pw_set("TeamB2", "mcp3202")
        self.client.connect(self.address)


    def publish_value(self, value):
        # Publishing the voltage read from sensors
        self.client.publish(self.topic_values, f"{value}")

    def send_error(self, error_message):
        # Publish error message to broker
        self.client.publish(self.topic_errors, error_message)
