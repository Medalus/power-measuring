Step 1. git checkout -b <branch_name>  
Step 2. Make the changes you need to make  
Step 3. git add .  
Step 4. git commit -m "My commmit message"  
Step 5. git push <remote_name> <branch_name>  
In the output, GitLab prompts you with a direct link for creating a merge request:  
Copy that link and paste it in your browser, and the New Merge Request page is displayed.  
Step 6. Fill out the merge request  
Step 7. Wait for the merge to be accepted  
Step 8. git branch -d <branch_name>