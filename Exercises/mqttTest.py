'''
Inspiration from http://www.steves-internet-guide.com/into-mqtt-python-client/
created by: Nikolaj Simonsen | february 2021
repo: https://gitlab.com/npes-py-experiments/mqtt-tests 
'''

from socket import error as socket_error
import paho.mqtt.client as mqtt
import time
from datetime import datetime
import random
import json

# variables
sensor_id = 1234
client_name = 'itt_client_1'
broker_name = 'skoledns1.northeurope.cloudapp.azure.com'  
#broker_name = 'nisimqtt.germanywestcentral.cloudapp.azure.com'
broker_port = 1883
topic1 = 'testtopic'
publish_interval = 1
client = mqtt.Client(client_name) #init client

# received message callback
def on_message(client, userdata, message):
    print('message received ', str(message.payload.decode('utf-8')))
    # print('message topic=',message.topic)
    # print('message qos=',message.qos)
    # print('message retain flag=',message.retain)

client.on_message=on_message # attach message callback to callback

# create payload
def build_payload(sensor_reading, sensor_id):
    timestamp = datetime.utcnow().timestamp() # utc timestamp
    payload = {'sensor_id': sensor_id, 'sensor_time': timestamp, 'sensor_value': sensor_reading}
    return json.dumps(payload) # convert to json format

# read the sensor
def read_sensor():
    sensor_reading = str(random.randrange(0,10))
    return sensor_reading

try: 
    print('connecting to broker')
    client.connect(broker_name, broker_port)
    print(f'connected to {broker_name} on port {broker_port} ')

except socket_error as e:
    print(f'could not connect {client_name} to {broker_name} on port {broker_port}\n {e}')
    exit(0)

# subscribe
client.loop_start() #start the loop
client.subscribe(topic1)
print(f'Subscribed to topics: {topic1}')

# publish
while(True):
    try:    
        payload = build_payload(read_sensor(), sensor_id)
        print(f'Publishing {payload} to topic, {topic1}')
        client.publish(topic1, payload)        
        time.sleep(publish_interval) # wait x seconds

    except KeyboardInterrupt: #cleanup nice
        client.loop_stop() #stop the loop
        exit(0)