from ww20functions import add
import unittest

result = 4

class TestAdd(unittest.TestCase):
	def test_equal(self):
		test = add(2,2)
		expected = 4
		self.assertEqual(test, expected, "Should be 4")	# ==

	def test_fail(self):
		test = add(2,2)
		expected = 8
		self.assertNotEqual(test, expected, "Should not be 8")	# !=

	def test_type(self):
		test = type(add(2,2))	#returns type and compares to expected type (int)
		expected = int
		self.assertEqual(test, expected, "Should be an integer")
if __name__ == "__main__":
	unittest.main()